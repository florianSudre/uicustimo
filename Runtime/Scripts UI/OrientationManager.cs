﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
namespace UICustimoVisit
{

    public class OrientationManager : MonoBehaviour
    {
        [SerializeField] GameObject landScape;
        [SerializeField] GameObject portrait;

        public static bool IsActivate = false;
        // Update is called once per frame
        private void FixedUpdate()
        {
            switch (Screen.orientation)
            {
                case ScreenOrientation.Portrait:
                    portrait.SetActive(true);
                    landScape.SetActive(false);
                    break;
                case ScreenOrientation.PortraitUpsideDown:
                    portrait.SetActive(true);
                    landScape.SetActive(false);
                    break;
                case ScreenOrientation.LandscapeLeft:
                    portrait.SetActive(false);
                    landScape.SetActive(true);
                    break;
                case ScreenOrientation.LandscapeRight:
                    portrait.SetActive(false);
                    landScape.SetActive(true);
                    break;
                default:
                    break;
            }

            // float portraitvalue = portrait.GetComponent<ScrollRect>().verticalScrollbar.value;
            // float LandScapevalue = landScape.GetComponent<ScrollRect>().verticalScrollbar.value;
        }
    }
}
