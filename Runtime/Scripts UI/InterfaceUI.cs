﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace UICustimoVisit
{

    public interface IOpenableElement
    {
        bool IsOpenInt { get; set; }
        void OpenElement();
        void Rebuild();

    }
}
