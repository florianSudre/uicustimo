﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
namespace UICustimoVisit
{

    public class UiManager : MonoBehaviour
    {
        public static System.Action<List<GameObject>> Rebuild;
        [SerializeField] List<IOpenableElement> objectToReconstructOpen = new List<IOpenableElement>();
        [SerializeField] GameObject Canvas;
        public static UiManager instance = null;
        private void Awake()
        {
            if (instance != null)
            {
                Destroy(this);
                return;
            }
            instance = this;
            DontDestroyOnLoad(this);
            Rebuild += RebuilUIElements;

            RectTransform[] Allrect = FindObjectsOfType<RectTransform>();

            for (int i = 0; i < Allrect.Length; i++)
            {
                if (Allrect[i].GetComponent<IOpenableElement>() != null)
                {
                    objectToReconstructOpen.Add(Allrect[i].GetComponent<IOpenableElement>());
                }

            }
        }
        void RebuilUIElements(List<GameObject> _listUIElements)
        {
            foreach (GameObject element in _listUIElements)
            {
                LayoutRebuilder.ForceRebuildLayoutImmediate(element.GetComponent<RectTransform>());
            }
        }
        public void allreconstruct()
        {
            foreach (IOpenableElement item in objectToReconstructOpen)
            {
                item.Rebuild();
            }
        }
        public GameObject GetParentCanvas()
        {
            return Canvas;
        }
    }
}
