﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
namespace UICustimoVisit
{

    [RequireComponent(typeof(RectTransform))]
    public class SafeAreaScript : MonoBehaviour
    {
        // Start is called before the first frame update

        private void Awake()
        {
            RectTransform RectTrans = GetComponent<RectTransform>();
            Rect SafeArea = Screen.safeArea;

            Vector2 ancorMin = SafeArea.position;
            Vector2 ancorMax = ancorMin + SafeArea.size;

            ancorMin.x /= Screen.width;
            ancorMin.y /= Screen.height;
            ancorMax.x /= Screen.width;
            ancorMax.y /= Screen.height;

            RectTrans.anchorMin = ancorMin;
            RectTrans.anchorMax = ancorMax;
        }
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {

        }
    }
}