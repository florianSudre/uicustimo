﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;

namespace UICustimoVisit
{

    public class SyncProtraitLandscape : MonoBehaviour
    {
        public Dictionary<string, bool> ListActivableWidget = new Dictionary<string, bool>();

        public List<GameObject> test = new List<GameObject>();

        [SerializeField] ScrollRect[] listScrollBar = new ScrollRect[2];

        public static SyncProtraitLandscape instance = null;

        public float ScrollValue = 0.0f;
        // Start is called before the first frame update
        private void Awake()
        {
            if (instance != null)
            {
                Destroy(this);
                return;
            }
            instance = this;
            DontDestroyOnLoad(this);
        }
        void Start()
        {
            listScrollBar[0].onValueChanged.AddListener(RescrollProtrait);
            listScrollBar[1].onValueChanged.AddListener(RescrollLandScape);
            StartCoroutine(testcouroutine());
        }
        IEnumerator testcouroutine()
        {
            yield return new WaitForSeconds(1f);
            OpenText[] listText = FindObjectsOfType<OpenText>();
            DetailPanel[] DetailText = FindObjectsOfType<DetailPanel>();

            for (int i = 0; i < listText.Length; i++)
            {
                ListActivableWidget.Add(listText[i].gameObject.GetComponent<OpenText>().ID, listText[i].IsOpenInt);
                test.Add(listText[i].gameObject);
            }
            for (int i = 0; i < DetailText.Length; i++)
            {
                ListActivableWidget.Add(DetailText[i].gameObject.GetComponent<DetailPanel>().ID, DetailText[i].IsOpenInt);
                test.Add(DetailText[i].gameObject);
            }
        }


        void RescrollProtrait(Vector2 temp)
        {
            ScrollValue = temp.y;
        }
        void RescrollLandScape(Vector2 temp)
        {
            ScrollValue = temp.y;
        }
        public float GetScrollValue()
        {
            return ScrollValue;
        }
        public void OpenElementOnCall(string _Id, bool _value)
        {
            ListActivableWidget[_Id] = _value;
        }
        public bool GetValue(string _Id)
        {
            return ListActivableWidget[_Id];
        }
    }
}