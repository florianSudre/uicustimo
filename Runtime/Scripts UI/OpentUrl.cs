﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace UICustimoVisit
{

    public class OpentUrl : MonoBehaviour
    {
        [SerializeField] string url;

        public void OpenUrl()
        {
            Application.OpenURL(url);
        }
    }
}
