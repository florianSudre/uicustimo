﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
namespace UICustimoVisit
{

    public class ScrollScript : MonoBehaviour
    {
        // Start is called before the first frame update
        private void OnEnable()
        {
            if (GetComponent<ScrollRect>() != null && SyncProtraitLandscape.instance != null)
                GetComponent<ScrollRect>().verticalScrollbar.value = SyncProtraitLandscape.instance.ScrollValue;
        }
    }
}