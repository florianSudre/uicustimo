﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
namespace UICustimoVisit
{

    public class GalleryScript : MonoBehaviour
    {
        public List<Sprite> Images = new List<Sprite>();
        [SerializeField] GameObject ZoomViewer;
        [SerializeField] Image ZoomImage;
        [SerializeField] Image RefCurrentImage;
        [SerializeField] Image LastImage;
        [SerializeField] Image Tuto;

        [SerializeField] float SpeedFade = 0f;
        public int CurrentImageIndex = 0;

        public bool switching;

        public System.Action<int> SwitchNewImage;

        public System.Action addnewDot;

        float time;
        public float LimitTimeTuto;
        bool tutoIsViewing;
        // Start is called before the first frame update
        void Start()
        {
        }

        // Update is called once per frame
        void Update()
        {
            time += Time.deltaTime;
            if (!tutoIsViewing && time > LimitTimeTuto)
                ViewTuto();
        }

        public void SwitchImage(int _decal)
        {
            LastImage.sprite = Images[CurrentImageIndex];
            LastImage.CrossFadeAlpha(1, 0, false);
            CurrentImageIndex += _decal;
            if (CurrentImageIndex < 0)
                CurrentImageIndex = Images.Count - 1;
            if (CurrentImageIndex > Images.Count - 1)
                CurrentImageIndex = 0;
            RefCurrentImage.sprite = Images[CurrentImageIndex];
            ZoomImage.sprite = RefCurrentImage.sprite;
            LastImage.CrossFadeAlpha(0, SpeedFade, false);
            SwitchNewImage.Invoke(CurrentImageIndex);
            Tuto.gameObject.SetActive(false);
            time = 0;
        }

        public void switchZoomViewer()
        {
            ZoomViewer.SetActive(!ZoomViewer.activeSelf);
        }

        void ViewTuto()
        {
            Tuto.gameObject.SetActive(true);
        }

        public void AddImage(Sprite _newSprite)
        {
            Images.Add(_newSprite);
            if (Images.Count == 1)
            {
                RefCurrentImage.sprite = Images[0];
                LastImage.sprite = Images[0];
                LastImage.CrossFadeAlpha(0, 0, false);
            }
            addnewDot.Invoke();
        }
    }
}
