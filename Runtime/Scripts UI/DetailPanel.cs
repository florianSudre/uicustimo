﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace UICustimoVisit
{

    public class DetailPanel : MonoBehaviour, IOpenableElement
    {
        public string ID = "11";
        [SerializeField] List<GameObject> objectToReconstruct = new List<GameObject>();
        [SerializeField] GameObject ListUIObject;
        [SerializeField] Sprite upImage;
        [SerializeField] Sprite DownImage;
        [SerializeField] Image ArrowImage;

        public bool IsOpen;
        public bool IsOpenInt { get { return IsOpen; } set { IsOpen = value; } }
        bool start = false;
        private void Awake()
        {
            IsOpenInt = false;
        }
        private void Start()
        {
            objectToReconstruct = UIFacility.GetAllParentUnderCanvas(this.gameObject, UiManager.instance.GetParentCanvas());
            if (UiManager.instance != null)
                UiManager.Rebuild.Invoke(objectToReconstruct);
            start = true;
        }
        private void OnEnable()
        {
            if (start)
            {
                if (SyncProtraitLandscape.instance?.GetValue(ID) != IsOpen)
                {
                    OpenElement();
                }
                UiManager.instance.allreconstruct();
            }
        }
        public void Rebuild()
        {
            if (UiManager.instance != null)
            {
                UiManager.Rebuild.Invoke(objectToReconstruct);
                UiManager.Rebuild.Invoke(objectToReconstruct);
            }
            else
                print("ERROR NO UIManager");

        }
        public void OpenElement()
        {
            IsOpen = !IsOpen;
            ArrowImage.sprite = IsOpen ? upImage : DownImage;
            ListUIObject.SetActive(IsOpen);
            Rebuild();
            SyncProtraitLandscape.instance?.OpenElementOnCall(ID, IsOpen);
        }
    }
}

