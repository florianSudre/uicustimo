﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
namespace UICustimoVisit
{

    public static class UIFacility
    {
        public static List<GameObject> GetAllParentUnderCanvas(GameObject _self, GameObject LimitParent)
        {
            List<GameObject> tempListGameObject = new List<GameObject>();

            RecursiveGetParents(_self, tempListGameObject, LimitParent);

            return tempListGameObject;
        }
        public static void RecursiveGetParents(GameObject currentgameobject, List<GameObject> _parents, GameObject _Limit)
        {
            if (currentgameobject != _Limit)
            {
                if (currentgameobject.GetComponent<RectTransform>())
                {
                    _parents.Add(currentgameobject.GetComponent<RectTransform>().parent.gameObject);
                }
                RecursiveGetParents(_parents[_parents.Count - 1], _parents, _Limit);
            }
        }

    }

}