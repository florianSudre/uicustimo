﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
namespace UICustimoVisit
{

    public class SelectedImageZoomUi : MonoBehaviour
    {
        [SerializeField] float ZoomMax;
        [SerializeField] ScrollRect scroll;
        [SerializeField] RectTransform parentrect;
        [SerializeField] Text test;

        float WidthStart;
        float heightStart;
        float currentWidth;
        float MaxWidth;
        float Maxheight;
        float MinWidth;
        public float MinHeight;
        // Start is called before the first frame update
        void Start()
        {


        }
        void Update()
        {
            if (Input.touchCount == 2)
            {
                scroll.enabled = false;
                Touch touchZero = Input.GetTouch(0);
                Touch touchOne = Input.GetTouch(1);

                Vector2 touchZeroPrevPos = touchZero.position - touchZero.deltaPosition;
                Vector2 touchOnePrevPos = touchOne.position - touchOne.deltaPosition;

                float prevMagnitude = (touchZeroPrevPos - touchOnePrevPos).magnitude;
                float CurrentMagnitude = (touchZero.position - touchOne.position).magnitude;

                float diffMagnitude = CurrentMagnitude - prevMagnitude;

                ZoomImage(diffMagnitude);

            }
            else
            {
                scroll.enabled = true;
            }
        }

        void ZoomImage(float Increment)
        {
            RectTransform currentRect = GetComponent<RectTransform>();
            MaxWidth = parentrect.rect.width * ZoomMax;
            Maxheight = parentrect.rect.height * ZoomMax;

            MinWidth = parentrect.rect.width;
            MinHeight = parentrect.rect.height;
            currentRect.sizeDelta = new Vector2(Mathf.Clamp(currentRect.rect.width + Increment, MinWidth, MaxWidth), Mathf.Clamp(currentRect.rect.height + Increment, MinHeight, Maxheight));
        }

    }

}