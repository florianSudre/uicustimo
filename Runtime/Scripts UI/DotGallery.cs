﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace UICustimoVisit
{

    public class DotGallery : MonoBehaviour
    {
        [SerializeField] GalleryScript gallery;
        [SerializeField] GameObject PrefabDots;
        public List<Image> DotsList = new List<Image>();
        Sprite[] spriteDots = new Sprite[2];

        public Vector2 SizeDot;
        int Currentindex = 0;
        // Start is called before the first frame update
        void Start()
        {
            for (int i = 0; i < gallery.Images.Count; i++)
            {
                InstanciateDots();
            }
            Currentindex = gallery.CurrentImageIndex;
            gallery.SwitchNewImage += SwitchIndex;
            gallery.addnewDot += InstanciateDots;
        }


        // Update is called once per frame
        void Update()
        {

        }

        public void InstanciateDots()
        {
            GameObject temp = Instantiate(PrefabDots, this.transform);
            DotsList.Add(temp.GetComponent<Image>());
            temp.GetComponent<RectTransform>().sizeDelta = SizeDot;
            if (DotsList.Count == 1)
            {
                DotsList[gallery.CurrentImageIndex].color = new Color(0, 0, 0, 1);
            }
        }

        void SwitchIndex(int _index)
        {
            DotsList[Currentindex].color = new Color(1, 1, 1, 1);
            Currentindex = _index;
            DotsList[Currentindex].color = new Color(0, 0, 0, 1);
        }




    }
}