﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
namespace UICustimoVisit
{

    public class OpenText : MonoBehaviour, IOpenableElement
    {
        public string ID = "00";

        [SerializeField] List<GameObject> objectToReconstruct = new List<GameObject>();
        [SerializeField] int NbrLimitCharacter;
        [SerializeField] VerticalLayoutGroup RectTarget;
        Text originalText;
        string HideText;
        string fullText;

        public bool IsOpen;
        public bool IsOpenInt { get { return IsOpen; } set { IsOpen = value; } }
        bool start = false;
        void Start()
        {
            objectToReconstruct = UIFacility.GetAllParentUnderCanvas(this.gameObject, UiManager.instance.GetParentCanvas());
            originalText = GetComponent<Text>();
            if (UiManager.instance != null)
                UiManager.Rebuild.Invoke(objectToReconstruct);
            start = true;
        }
        private void OnEnable()
        {
            if (start)
            {

                if (SyncProtraitLandscape.instance.GetValue(ID) != IsOpen)
                {
                    OpenElement();
                }
                UiManager.instance.allreconstruct();

                UiManager.instance.allreconstruct();
            }

        }
        public void OpenElement()
        {
            if (originalText != null)
                originalText.text = IsOpen ? HideText : fullText;
            IsOpen = !IsOpen;
            Rebuild();
            SyncProtraitLandscape.instance?.OpenElementOnCall(ID, IsOpen);
        }
        public void Rebuild()
        {
            if (UiManager.instance != null)
                UiManager.Rebuild.Invoke(objectToReconstruct);
            UiManager.Rebuild.Invoke(objectToReconstruct);
        }
        public void RefreshTextWithNewData(string _NewText)
        {
            string temp = _NewText;
            fullText = temp;
            if (NbrLimitCharacter > fullText.Length)//Secure Limit character in string
            {
                NbrLimitCharacter = fullText.Length;
                HideText = fullText;
            }
            else
            {
                string temp2 = temp.Substring(0, NbrLimitCharacter);
                HideText = temp2 + "...(click on it to see more)";
            }
            originalText.text = HideText;
            Rebuild();
        }


    }

}